<?php
	foreach($users_data as $row){
?>
    <div class="tab-pane fade active in" id="edit">
        <?php
			echo form_open(base_url() . 'index.php/admin/admins/update/' . $row['user_id'], array(
				'class' => 'form-horizontal',
				'method' => 'post',
				'id' => 'admin_edit'
			));
		?>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-1">
                    	<?php echo translate('first_name'); ?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="first_name" value="<?php echo ucfirst($row['first_name']); ?>" id="demo-hor-1" class="form-control required" placeholder="<?php echo translate('First name'); ?>" >
                    </div>
                </div>
				
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-10">
                    	<?php echo translate('last_name'); ?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" name="last_name" value="<?php echo ucfirst($row['last_name']); ?>" id="demo-hor-10" class="form-control required" placeholder="<?php echo translate('Last name'); ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-2">
                    	<?php echo translate('email'); ?>
                    </label>
                    <div class="col-sm-6">
                        <?php echo $row['user_email']; ?>
                		<div class="label label-danger" style="display:none;" id='email_note'></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-3">
                        <?php echo translate('phone_no'); ?>
                    </label>
                    <div class="col-sm-6">
						 <?php echo $row['phone_no']; ?>
                        <div class="label label-danger" style="display:none;" id='email_note'></div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-5">
                    	<?php echo translate('country'); ?>
                    </label>
                    <div class="col-sm-6">
                         <input type="text" name="country" value="<?php echo $row['country']; ?>" id="demo-hor-4" class="form-control" placeholder="<?php echo translate('country'); ?>" >
                    </div>
                    </div>
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-6">
                    	<?php echo translate('state'); ?>
                    </label>
                    <div class="col-sm-6">
                         <input type="text" name="state" value="<?php echo $row['state']; ?>" id="demo-hor-6" class="form-control" placeholder="<?php echo translate('state'); ?>" >
                    </div>
                    </div>
				
				
				
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-8">
                    	<?php echo translate('city'); ?>
                    </label>
                    <div class="col-sm-6">
                         <input type="text" name="city" value="<?php echo $row['city']; ?>" id="demo-hor-8" class="form-control" placeholder="<?php echo translate('city'); ?>" >
                    </div>
                    </div>
				
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-5">
                    	<?php echo translate('zipcode'); ?>
                    </label>
                    <div class="col-sm-6">
                         <input type="text" name="zipcode" value="<?php echo $row['zipcode']; ?>" id="demo-hor-4" class="form-control" placeholder="<?php echo translate('zipcode'); ?>" >
                    </div>
                    </div>
				<div class="form-group">
                    <label class="col-sm-4 control-label" for="demo-hor-9">
                    	<?php echo translate('address'); ?>
                    </label>
                    <div class="col-sm-6">
                         <textarea name="address" id="demo-hor-9" class="form-control"><?php echo $row['address']; ?></textarea>
                    </div>
                    </div>
				
				
                </div>
                
            </div>
    	</form>
    </div>
<?php
	}
?>
<script>
	$(document).ready(function() {
		$("form").submit(function(e){
			return false;
		});
		$('.demo-chosen-select').chosen();
		$('.demo-cs-multiselect').chosen({width:'100%'});
	});
	
	
	$(".emails").blur(function(){
		var email = $(".emails").val();
		$.post("<?php echo base_url(); ?>index.php/admin/exists",
		{
			<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>',
			email: email
		},
		function(data, status){
			if(data == 'yes'){
				$("#email_note").show();
				$("#email_note").html('*<?php echo 'email_already_registered'; ?>');
				$("body .modal-dialog .btn-purple").addClass("disabled");
			} else if(data == 'no'){
				$("#email_note").hide();
				$("#email_note").html('');
				$("body .modal-dialog .btn-purple").removeClass("disabled");
			}
		});
	});
	
</script>