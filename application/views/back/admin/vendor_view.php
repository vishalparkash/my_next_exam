<?php 
	foreach($book_data as $row)
	{ 
?>
    <div id="content-container" style="padding-top:0px !important;">
       
    <div class="text-center pad-all">
            <div class="pad-ver">
                <img 
                    <?php if(file_exists('uploads/books_image/banner_'.$row['user_id'].'.jpg')){ ?>
                        src="<?php echo base_url(); ?>uploads/books_image/banner_<?php echo $row['user_id']; ?>.jpg"
                 
					<?php } else { ?>
                        src="<?php echo base_url(); ?>uploads/user_image/default.jpg"
                    <?php } ?>
                    class="img-md img-border img-circle" alt="Profile Picture">
            </div>
            <h4 class="text-lg text-overflow mar-no"><?php echo $row['book_name']?></h4>
            <p class="text-sm"><?php echo translate('Book');?></p>
          
            <hr>
        </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="panel-body">
                <table class="table table-striped" style="border-radius:3px;">
                    <tr>
                        <th class="custom_td"><?php echo translate('book_name');?></th>
                        <td class="custom_td"><?php echo $row['book_name'] ?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('author');?></th>
                        <td class="custom_td">
                            <?php echo $row['author']?>
                         
                        </td>
                    </tr>
					  <tr>
                        <th class="custom_td"><?php echo translate('edition');?></th>
                        <td class="custom_td">
                            <?php echo $row['edition']?>
                         
                        </td>
                    </tr>
					  <tr>
                        <th class="custom_td"><?php echo translate('subject');?></th>
                        <td class="custom_td">
                            <?php echo $row['subject']?>
                         
                        </td>
                    </tr>
					
					  <tr>
                        <th class="custom_td"><?php echo translate('book_class');?></th>
                        <td class="custom_td">
                            <?php echo $row['book_class']?>
                         
                        </td>
                    </tr>
					
					  <tr>
                        <th class="custom_td"><?php echo translate('book_condition');?></th>
                        <td class="custom_td">
                            <?php echo $row['book_condition']?>
                         
                        </td>
                    </tr>
					
					<tr>
                        <th class="custom_td"><?php echo translate('expected_price');?></th>
                        <td class="custom_td">
                            <?php echo $row['expected_price']?>
                         
                        </td>
                    </tr>
					
					
                    <tr>
                        <th class="custom_td"><?php echo translate('email');?></th>
                        <td class="custom_td"><?php echo $row['email']?></td>
                    </tr>
                    <tr>
                        <th class="custom_td"><?php echo translate('phone_number');?></th>
                        <td class="custom_td"><?php echo $row['phone_no']?></td>
                    </tr>
                  
                    <tr>
                        <th class="custom_td"><?php echo translate('Added On');?></th>
                        <td class="custom_td"><?php echo date('d M,Y',strtotime($row['created_at']));?></td>
                    </tr>
                </table>
              </div>
            </div>
        </div>					
    </div>					
<?php 
	}
?>
            
<style>
.custom_td{
border-left: 1px solid #ddd;
border-right: 1px solid #ddd;
border-bottom: 1px solid #ddd;
}
</style>
<script>
$(document).ready(function(e) {
    $('.modal-footer').find('.btn-purple').hide();
});
</script>