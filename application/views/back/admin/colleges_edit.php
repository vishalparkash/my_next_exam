	<?php
//error_reporting(0);
	foreach($college_data as $row){
	/*echo "<pre>";
	print_r($row);die;*/
?>
    <div>
        <?php
			echo form_open(base_url() . 'index.php/admin/colleges/update/' . $row['college_id'], array(
				'class' => 'form-horizontal',
				'method' => 'post',
				'id' => 'colleges_edit',
				'enctype' => 'multipart/form-data'
			));
		?>
	
            <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('College Name');?></label>
                <div class="col-sm-6">
                    <input type="text" name="college_name" id="demo-hor-1" 
                    	placeholder="<?php echo translate('College name'); ?>" class="form-control required" value="<?php echo $row['college_name'];?>">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="intro"><?php echo translate('Introduction');?></label>
                <div class="col-sm-6">
                    <input type="text" name="intro" id="intro" placeholder="<?php echo translate('Introduction'); ?>" class="form-control required" value="<?php echo $row['intro'];?>">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="estd_year"><?php echo translate('estd_year');?></label>
                <div class="col-sm-6">
                    <input type="text" name="estd_year" id="estd_year" pattern="\d{1,4}" title="Year should be in numberic" placeholder="<?php echo translate('Establishment Year'); ?>" class="form-control required" value="<?php echo $row['estd_year'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="university"><?php echo translate('university');?></label>
                <div class="col-sm-6">
                    <input type="text" name="university" id="university" placeholder="<?php echo translate('university'); ?>" class="form-control required" value="<?php echo $row['university'];?>">
                </div>
            </div>
			
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="college_address"><?php echo translate('college_address');?></label>
                 <div class="col-sm-6">
                                    <textarea rows="9"  class="summernotes" name="college_address" data-height="200" data-name="description"><?php echo $row['college_address'];?></textarea>
                                </div>
            </div>
			
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="city"><?php echo translate('City');?></label>
                <div class="col-sm-6">
                    <input type="text" name="city" id="city" placeholder="<?php echo translate('City'); ?>" id="city" class="form-control required" value="<?php echo $row['city'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="state"><?php echo translate('State');?></label>
                <div class="col-sm-6">
                    <input type="text" name="state" id="state" placeholder="<?php echo translate('State'); ?>" class="form-control required" value="<?php echo $row['state'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="college_image"><?php echo translate('College Image');?></label>
                <div class="col-sm-6">
                    <input type="file" name="college_image" id="college_image" 
                    	placeholder="<?php echo translate('College Image'); ?>" class="form-control required">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="zipcode"><?php echo translate('zipcode');?></label>
                <div class="col-sm-6">
                    <input type="text" name="zipcode" id="zipcode" pattern="\d{1,6}" title="Zipcode should be in numberic" placeholder="<?php echo translate('zipcode'); ?>" class="form-control required" value="<?php echo $row['zipcode'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="logo_url"><?php echo translate('College logo URL');?></label>
                <div class="col-sm-6">
                    <input type="text" name="logo_url" id="logo_url" value="<?php echo $row['logo_url'];?>" placeholder="<?php echo translate('College logo URL'); ?>" class="form-control required">
                </div>
            </div>
			
			
				<div class="form-group">
                <label class="col-sm-4 control-label" for="ranking"><?php echo translate('College ranking');?></label>
                <div class="col-sm-6">
                    <input type="text" name="ranking" pattern="\d{1,5}" title="Ranking should be in numberic" id="ranking" placeholder="<?php echo translate('College ranking'); ?>" class="form-control required" value="<?php echo $row['ranking'];?>">
                </div>
            </div>
			
			
				<div class="form-group">
                <label class="col-sm-4 control-label" for="college_email"><?php echo translate('College email');?></label>
                <div class="col-sm-6">
                    <input type="email" name="college_email" id="college_email" placeholder="<?php echo translate('College Email'); ?>" class="form-control required" value="<?php echo $row['college_email'];?>">
                </div>
            </div>
			
			
				<div class="form-group">
                <label class="col-sm-4 control-label" for="college_phone"><?php echo translate('College Phone');?></label>
                <div class="col-sm-6">
                    <input type="text" name="college_phone" id="college_phone" placeholder="<?php echo translate('College Phone'); ?>" class="form-control required" pattern="\d{1,10}" title="Only numbers allowed" value="<?php echo $row['college_phone'];?>">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="college_fax"><?php echo translate('College Fax');?></label>
                <div class="col-sm-6">
                    <input type="text" name="college_fax" id="college_fax" placeholder="<?php echo translate('College Fax'); ?>" class="form-control required" value="<?php echo $row['college_fax'];?>">
                </div>
            </div>
			
			
			<!--div class="form-group">
                <label class="col-sm-4 control-label" for="seats"><?php //echo translate('Seats');?></label>
                <div class="col-sm-6">
                    <input type="text" name="seats" id="seats" 
                    	placeholder="<?php //echo translate('Seats'); ?>" class="form-control required">
                </div>
            </div-->
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="website"><?php echo translate('website');?></label>
                <div class="col-sm-6">
                    <input type="text" name="website" id="website" placeholder="<?php echo translate('website'); ?>" class="form-control required" value="<?php echo $row['website'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="prospectus"><?php echo translate('prospectus URL');?></label>
                <div class="col-sm-6">
                    <input type="text" name="prospectus" id="prospectus" placeholder="<?php echo translate('prospectus URL'); ?>" class="form-control required" value="<?php echo $row['prospectus'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="recognition_status"><?php echo translate('recognition_status');?></label>
                <div class="col-sm-6">
                    <input type="text" name="recognition_status" id="recognition_status" placeholder="<?php echo translate('recognition_status'); ?>" class="form-control required" value="<?php echo $row['recognition_status'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="college_latitude"><?php echo translate('college_latitude');?></label>
                <div class="col-sm-6">
                    <input type="text" name="college_latitude" id="college_latitude" placeholder="<?php echo translate('college_latitude'); ?>" class="form-control required" value="<?php echo $row['college_latitude'];?>">
                </div>
            </div>
			
			<div class="form-group">
                <label class="col-sm-4 control-label" for="college_longitude"><?php echo translate('college_longitude');?></label>
                <div class="col-sm-6">
                    <input type="text" name="college_longitude" id="college_longitude" placeholder="<?php echo translate('college_longitude'); ?>" class="form-control required" value="<?php echo $row['college_longitude'];?>">
                </div>
            </div>
			
			
            
			
           
        </div>
        </form>
    </div>

<?php
	}
?>


<style>
.course_select{width: 100%;}
.summernotes{width: 100%;}
</style>
