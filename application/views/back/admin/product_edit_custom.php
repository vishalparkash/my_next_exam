
<div class="row">
    <div class="col-md-12">
		<?php
            echo form_open(base_url() . 'index.php/admin/product/do_add/', array(
                'class' => 'form-horizontal',
                'method' => 'post',
                'id' => 'product_add',
				'enctype' => 'multipart/form-data'
            ));
        ?>
            <!--Panel heading-->
            <div class="panel-heading">
                <div class="panel-control" style="float: left;">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#product_details"><?php echo translate('entrance_examinations'); ?></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#business_details"><?php echo translate('Exam Form Details'); ?></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#customer_choice_options"><?php echo translate('Exam Centers'); ?></a>
                        </li>
                         <li>
                            <a data-toggle="tab" href="#customer_choice_options_new"><?php echo translate('Exam Counselling'); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
                <div class="tab-base">
                    <!--Tabs Content-->                    
                    <div class="tab-content">
                    	<div id="product_details" class="tab-pane fade active in">
        
                            <div class="form-group btm_border">
                                <h4 class="text-thin text-center"><?php echo translate('Exam Details'); ?></h4>                            
                            </div>
<div class="form-group">
                <label class="col-sm-4 control-label"><?php echo translate('courses');?></label>
                <div class="col-sm-6">
                    <?php echo $this->crud_model->select_html('courses','course','course_name','add','demo-chosen-select required','','',NULL); ?>
                </div>
            </div>
							
							<div class="form-group">
                <label class="col-sm-4 control-label"><?php echo translate('course_category');?></label>
                <div class="col-sm-6">
                    <?php echo $this->crud_model->select_html('course_category','course_category','category_name','add','demo-chosen-select required','','',NULL); ?>
                </div>
            </div>
							
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('exam_name');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_name" id="demo-hor-1" placeholder="<?php echo translate('exam_name');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_2"><?php echo translate('exam_shortname');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_shortname" id="demo-hor-ex_2" placeholder="<?php echo translate('exam_shortname');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('exam_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_date"  placeholder="<?php echo translate('exam_date');?>" class="form-control required" id='datepicker'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_3"><?php echo translate('introduction');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_intro" id="demo-hor-ex_3" placeholder="<?php echo translate('introduction');?>" class="form-control required">
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_4"><?php echo translate('other_information');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_other_info" id="demo-hor-ex_4" placeholder="<?php echo translate('other_information');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_5"><?php echo translate('eligibility_introduction');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_eligibility_intro" id="demo-hor-ex_5" placeholder="<?php echo translate('eligibility_introduction');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_6"><?php echo translate('eligibilities');?></label>
                                <div class="col-sm-6">
                                    
                                     <textarea rows="9" id="demo-hor-ex_6" name="exam_eligibilities"class="exam_text_area" data-height="200" data-name="exam_eligibilities"></textarea>
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_7"><?php echo translate('prospectus_introduction');?></label>
                                <div class="col-sm-6">
                                   
                                    <textarea rows="9" id="demo-hor-ex_7" name="exam_prospectus_intro"class="exam_text_area" data-height="200" data-name="exam_prospectus_intro"></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_8"><?php echo translate('prospectus_url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_prospectus_url" id="demo-hor-ex_8" placeholder="<?php echo translate('prospectus_url');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_9"><?php echo translate('prospectus_download_url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_prospectus_download_url" id="demo-hor-ex_9" placeholder="<?php echo translate('prospectus_download_url');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex_10"><?php echo translate('pattern_number_of_question');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_number_of_question" id="demo-hor-ex_10" placeholder="<?php echo translate('pattern_number_of_question');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex11"><?php echo translate('pattern_subjects');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_subjects" id="demo-hor-ex11" placeholder="<?php echo translate('pattern_subjects');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex12"><?php echo translate('pattern_language');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_language" id="demo-hor-ex12" placeholder="<?php echo translate('pattern_language');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex13"><?php echo translate('pattern_exam_mode');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_exam_mode" id="demo-hor-ex13" placeholder="<?php echo translate('pattern_exam_mode');?>" class="form-control required">
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex14"><?php echo translate('pattern_duration');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_duration" id="demo-hor-ex14" placeholder="<?php echo translate('pattern_duration');?>" class="form-control required">
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex15"><?php echo translate('pattern_duration_text');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_duration_text" id="demo-hor-ex15" placeholder="<?php echo translate('pattern_duration_text');?>" class="form-control required">
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex16"><?php echo translate('pattern_marking');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_marking" id="demo-hor-ex16" placeholder="<?php echo translate('pattern_marking');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex17"><?php echo translate('pattern_negative_marking');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_negative_marking" id="demo-hor-ex17" placeholder="<?php echo translate('pattern_negative_marking');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex18"><?php echo translate('pattern_sample_paper');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_pattern_sample_paper" id="demo-hor-ex18" placeholder="<?php echo translate('pattern_sample_paper');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex19"><?php echo translate('pattern_requirements');?></label>
                                <div class="col-sm-6">
                             
                                    
                                     <textarea rows="9" id="demo-hor-ex19" name="exam_pattern_requirements"class="exam_text_area" data-height="200" data-name="exam_pattern_requirements"></textarea>
                                </div>
                            </div>
                            
                             
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker2"><?php echo translate('admit_card_issue_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_admit_card_issue_date"  placeholder="<?php echo translate('admit_card_issue_date');?>" class="form-control required" id='datepicker2'>
                                    
                                </div>
                            </div>
                            
                               <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker3"><?php echo translate('admit_card_download_last_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_admit_card_download_last_date"  placeholder="<?php echo translate('admit_card_download_last_date');?>" class="form-control required" id='datepicker3'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex20"><?php echo translate('admit_card_instructions');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_admit_card_instructions"  placeholder="<?php echo translate('admit_card_instructions');?>" class="form-control required" id="demo-hor-ex20">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex21"><?php echo translate('admit_card_dress_code');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_admit_card_dress_code" id="demo-hor-ex21" placeholder="<?php echo translate('admit_card_dress_code');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                              <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex22"><?php echo translate('admit_card_requirements');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_admit_card_requirements" id="demo-hor-ex22" placeholder="<?php echo translate('admit_card_requirements');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex23"><?php echo translate('syllabus_description');?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9"  name="exam_syllabus_desc" id="demo-hor-ex23" class="exam_text_area required" data-height="200" data-name="exam_syllabus_desc"></textarea>
                                    
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex24"><?php echo translate('syllabus_points');?></label>
                                <div class="col-sm-6">
                                  
                                    <textarea rows="9"  name="exam_syllabus_points" id="demo-hor-ex24" class="exam_text_area required" data-height="200" data-name="exam_syllabus_points"></textarea>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex25"><?php echo translate('syllabus_Url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_syllabus_link" id="demo-hor-ex25" placeholder="<?php echo translate('syllabus_Url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-26"><?php echo translate('syllabus_download_Url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_syllabus_download_link" id="demo-hor-ex26" placeholder="<?php echo translate('syllabus_download_Url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex27"><?php echo translate('result_intro');?></label>
                                <div class="col-sm-6">
                                 <textarea rows="9"  name="exam_result_intro" id="demo-hor-ex27" class="exam_text_area required" data-height="200" data-name="exam_result_intro"></textarea>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex28"><?php echo translate('result_intro_points');?></label>
                                <div class="col-sm-6">
                                 <textarea rows="9"  name="exam_result_intro_points" id="demo-hor-ex28" class="exam_text_area required" data-height="200" data-name="exam_result_intro_points"></textarea>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex29"><?php echo translate('exam_result_date');?></label>
                                <div class="col-sm-6">
                                  <input type="text" name="exam_result_date" id="demo-hor-ex29" placeholder="<?php echo translate('exam_result_date');?>" class="form-control required" id='datepicker4'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex30"><?php echo translate('exam_result_url');?></label>
                                <div class="col-sm-6">
                                  <input type="text" name="exam_result_link" id="demo-hor-ex30"  placeholder="<?php echo translate('exam_result_url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-ex31"><?php echo translate('OMR_sheet_download_date');?></label>
                                <div class="col-sm-6">
                                  <input type="text" name="download_omr_sheet_date" id="demo-hor-ex31" placeholder="<?php echo translate('OMR_sheet_download_date');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <!--div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" />
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div-->
                            
                            <?php /*<div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('exam_name');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_name" id="demo-hor-1" placeholder="<?php echo translate('exam_name');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('exam_name');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_name" id="demo-hor-1" placeholder="<?php echo translate('exam_name');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-2"><?php echo translate('category');?></label>
                                <div class="col-sm-6">
                                    <?php echo $this->crud_model->select_html('category','category','category_name','add','demo-chosen-select required','','digital',NULL,'get_cat'); ?>
                                </div>
                            </div>
                            
                            <div class="form-group btm_border" id="sub" style="display:none;">
                                <label class="col-sm-4 control-label" for="demo-hor-3"><?php echo translate('sub-category');?></label>
                                <div class="col-sm-6" id="sub_cat">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border" id="brn" style="display:none;">
                                <label class="col-sm-4 control-label" for="demo-hor-4"><?php echo translate('brand');?></label>
                                <div class="col-sm-6" id="brand">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-5"><?php echo translate('unit');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="unit" id="demo-hor-5" placeholder="<?php echo translate('unit_(e.g._kg,_pc_etc.)'); ?>" class="form-control unit required">
                                </div>
                            </div>              
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-11"><?php echo translate('tags');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="tag" data-role="tagsinput" placeholder="<?php echo translate('tags');?>" class="form-control">
                                </div>
                            </div>
                                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-12"><?php echo translate('images');?></label>
                                <div class="col-sm-6">
                                <span class="pull-left btn btn-default btn-file"> <?php echo translate('choose_file');?>
                                    <input type="file" multiple name="images[]" onchange="preview(this);" id="demo-hor-12" class="form-control required">
                                    </span>
                                    <br><br>
                                    <span id="previewImg" ></span>
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('description'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9" data-height="200" class="exam_text_area required" data-name="description"></textarea>
                                </div>
                            </div>
                            <?php */?>
                            <!--div id="more_additional_fields"></div>
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-inputpass"></label>
                                <div class="col-sm-6">
                                    <h4 class="pull-left">
                                        <i><?php //echo translate('if_you_need_more_field_for_your_product_,_please_click_here_for_more...');?></i>
                                    </h4>
                                    <div id="more_btn" class="btn btn-mint btn-labeled fa fa-plus pull-right">
                                    <?php //echo translate('add_more_fields');?></div>
                                </div>
                            </div-->
                            

                        </div>
                        <div id="business_details" class="tab-pane fade">
                            <div class="form-group btm_border">
                                <h4 class="text-thin text-center"><?php echo translate('Exam Form Details'); ?></h4>                            
                            </div>
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker6"><?php echo translate('start_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="start_date"  placeholder="<?php echo translate('start_date');?>" class="form-control required" id='datepicker6'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker7"><?php echo translate('last_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="last_date"  placeholder="<?php echo translate('last_date');?>" class="form-control required" id='datepicker7'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker8"><?php echo translate('last_date_with_late_fee');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="lastdate_with_late_fee"  placeholder="<?php echo translate('last_date_with_late_fee');?>" class="form-control required" id='datepicker8'>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="datepicker9"><?php echo translate('correction_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="correction_date"  placeholder="<?php echo translate('correction_date');?>" class="form-control required" id='datepicker9'>
                                    
                                </div>
                            </div>
                            
                           
                            
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('correction_text'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9" name="correction_text" id="demo-hor-13" data-height="200" class="exam_text_area required" data-name="correction_text"></textarea>
                                </div>
                            </div>
                            
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-14"><?php echo translate('requirements');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_requirements" id="demo-hor-14" placeholder="<?php echo translate('requirements');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                              <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-15"><?php echo translate('application_fee');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="application_fee" id="demo-hor-15" placeholder="<?php echo translate('application_fee');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-16"><?php echo translate('exam_Url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="exam_link" id="demo-hor-16"  placeholder="<?php echo translate('exam_Url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            
                            
                        </div>
                        <div id="customer_choice_options" class="tab-pane fade">
                            <div class="form-group btm_border">
                                <h4 class="text-thin text-center"><?php echo translate('Exam Centers'); ?></h4>                            
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="center_name"><?php echo translate('center_name');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="ex_center_center_name" id="center_name"  placeholder="<?php echo translate('center_name');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="center_code"><?php echo translate('center_code');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="ex_center_center_code" id="center_code"  placeholder="<?php echo translate('center_code');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="address"><?php echo translate('address');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="ex_center_address" id="address"  placeholder="<?php echo translate('address');?>" class="form-control required">
                                    
                                </div>
                            </div>
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="state"><?php echo translate('state');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="ex_center_state" id="state"  placeholder="<?php echo translate('state');?>" class="form-control required">
                                    
                                </div>
                            </div>
                           
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="city"><?php echo translate('city');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="ex_center_city" id="city"  placeholder="<?php echo translate('city');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="center_latitude"><?php echo translate('center_latitude');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="center_latitude" id="center_latitude"  placeholder="<?php echo translate('center_latitude');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="center_longitude"><?php echo translate('center_longitude');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="center_longitude" id="center_longitude"  placeholder="<?php echo translate('center_longitude');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            
                            
                        </div>
                        
                        <div id="customer_choice_options_new" class="tab-pane fade">
                            <div class="form-group btm_border">
                                <h4 class="text-thin text-center"><?php echo translate('Exam Counselling'); ?></h4>                            
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_intro"><?php echo translate('counselling_intro'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9" id="counselling_intro" data-height="200" name="counselling_intro" class="exam_text_area required" data-name="counselling_intro"></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="state"><?php echo translate('state');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_state" id="state"  placeholder="<?php echo translate('state');?>" class="form-control required">
                                    
                                </div>
                            </div>
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="association"><?php echo translate('association');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_association" id="association"  placeholder="<?php echo translate('association');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                              <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="category_intro"><?php echo translate('category_intro');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_category_intro" id="category_intro"  placeholder="<?php echo translate('category_intro');?>" class="form-control required">
                                    
                                </div>
                            </div>
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="category"><?php echo translate('category');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_category" id="category"  placeholder="<?php echo translate('category');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="source_info_link"><?php echo translate('Source Infomation Url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_source_info_link" id="source_info_link"  placeholder="<?php echo translate('Source Infomation Url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_form_start_date"><?php echo translate('counselling_form_start_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_form_start_date" id="counselling_form_start_date"  placeholder="<?php echo translate('counselling_form_start_date');?>" class="form-control required">
                                    
                                </div>
                            </div>
                             
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_form_last_date"><?php echo translate('counselling_form_last_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_form_last_date" id="counselling_form_last_date"  placeholder="<?php echo translate('counselling_form_last_date');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="online_form_link"><?php echo translate('online_form_Url');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="online_form_link" id="online_form_link"  placeholder="<?php echo translate('online_form_Url');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_form_result_date"><?php echo translate('counselling_form_result_date');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_form_result_date" id="counselling_form_result_date"  placeholder="<?php echo translate('counselling_form_result_date');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="univercity_name"><?php echo translate('university_name');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_univercity_name" id="univercity_name"  placeholder="<?php echo translate('university_name');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_open"><?php echo translate('counselling_open');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_open" id="counselling_open"  placeholder="<?php echo translate('counselling_open');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_merit_list"><?php echo translate('counselling_merit_list');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="counselling_merit_list" id="counselling_merit_list"  placeholder="<?php echo translate('counselling_merit_list');?>" class="form-control required">
                                    
                                </div>
                            </div>
                            
                            
                              <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="counselling_requirement"><?php echo translate('counselling_requirement');?></label>
                                <div class="col-sm-6">
                                    
                                    
                                     <textarea rows="9" id="counselling_requirement" name="counselling_requirement" data-height="200" class="exam_text_area required" data-name="counselling_requirement"></textarea>
                                    
                                </div>
                            </div>
                            
                            
                            
                             <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="document_required"><?php echo translate('documents_required'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9" id="document_required" name="document_required" data-height="200" class="exam_text_area required" data-name="documents_required"></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="other_text"><?php echo translate('other_text'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9" id="other_text" name="other_text" data-height="200" class="exam_text_area required" data-name="other_text"></textarea>
                                </div>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>

                <span class="btn btn-purple btn-labeled fa fa-hand-o-right pull-right" onclick="next_tab()"><?php echo translate('next'); ?></span>
                <span class="btn btn-purple btn-labeled fa fa-hand-o-left pull-right" onclick="previous_tab()"><?php echo translate('previous'); ?></span>
        
            </div>
    
            <div class="panel-footer">
                <div class="row">
                	<div class="col-md-11">
                        <span class="btn btn-purple btn-labeled fa fa-refresh pro_list_btn pull-right " 
                            onclick="ajax_set_full('add','<?php echo translate('Add Exam'); ?>','<?php echo translate('successfully_added!'); ?>','product_add',''); "><?php echo translate('reset');?>
                        </span>
                    </div>
                    
                    <div class="col-md-1">
                        <span class="btn btn-success btn-md btn-labeled fa fa-upload pull-right enterer" onclick="form_submit('product_add','<?php echo translate('examination_has_been_uploaded!'); ?>');proceed('to_add');" ><?php echo translate('Add');?></span>
                    </div>
                    
                </div>
            </div>
    
        </form>
    </div>
</div>

<script src="<?php $this->benchmark->mark_time(); echo base_url(); ?>template/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js">
</script>

<input type="hidden" id="option_count" value="-1">

<script>
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }

    function other_forms(){}
	
	function set_summer(){
        $('.summernotes').each(function() {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
			if(now.closest('div').find('.val').length == 0){
            	now.closest('div').append('<input type="hidden" class="val" name="'+n+'">');
			}
            now.summernote({
                height: h,
                onChange: function() {
                    now.closest('div').find('.val').val(now.code());
                }
            });
            now.closest('div').find('.val').val(now.code());
        });
	}

    function option_count(type){
        var count = $('#option_count').val();
        if(type == 'add'){
            count++;
        }
        if(type == 'reduce'){
            count--;
        }
        $('#option_count').val(count);
    }

    function set_select(){
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
    }
	
    $(document).ready(function() {
        set_select();
		set_summer();
		createColorpickers();
	
    });
    $('#datepicker').datepicker();
    //$('#datepicker1').datepicker();
    $('#datepicker2').datepicker();
    $('#datepicker3').datepicker();
    $('#datepicker4').datepicker();
    $('#datepicker5').datepicker();
    $('#datepicker6').datepicker();
    $('#datepicker7').datepicker();
    $('#datepicker8').datepicker();
    $('#datepicker9').datepicker();
    $('#counselling_form_start_date').datepicker();
    $('#counselling_form_last_date').datepicker();
    $('#counselling_form_result_date').datepicker();
   /* $('#datepicker').datepicker();
    $('#datepicker').datepicker();*/
    function other(){
        set_select();
        $('#sub').show('slow');
    }
    function get_cat(id,now){
        $('#sub').hide('slow');
        ajax_load(base_url+'index.php/admin/product/sub_by_cat/'+id,'sub_cat','other');
    }
	function get_brnd(id){
        $('#brn').hide('slow');
        ajax_load(base_url+'index.php/admin/product/brand_by_sub/'+id,'brand','other');
        $('#brn').show('slow');
    }
    function get_sub_res(id){}

    $(".unit").on('keyup',function(){
        $(".unit_set").html($(".unit").val());
    });

	function createColorpickers() {
	
		$('.demo2').colorpicker({
			format: 'rgba'
		});
		
	}
    
    $("#more_btn").click(function(){
        $("#more_additional_fields").append(''
            +'<div class="form-group">'
            +'    <div class="col-sm-4">'
            +'        <input type="text" name="ad_field_names[]" class="form-control required"  placeholder="<?php echo translate('field_name'); ?>">'
            +'    </div>'
            +'    <div class="col-sm-5">'
            +'        <textarea rows="9"  class="summernotes" data-height="100" data-name="ad_field_values[]"></textarea>'
            +'    </div>'
            +'    <div class="col-sm-2">'
            +'        <span class="remove_it_v rms btn btn-danger btn-icon btn-circle icon-lg fa fa-times" onclick="delete_row(this)"></span>'
            +'    </div>'
            +'</div>'
        );
        set_summer();
    });
    
    function next_tab(){
        $('.nav-tabs li.active').next().find('a').click();                    
    }
    function previous_tab(){
        $('.nav-tabs li.active').prev().find('a').click();                     
    }
    
    $("#more_option_btn").click(function(){
        option_count('add');
        var co = $('#option_count').val();
        $("#more_additional_options").append(''
            +'<div class="form-group" data-no="'+co+'">'
            +'    <div class="col-sm-4">'
            +'        <input type="text" name="op_title[]" class="form-control required"  placeholder="<?php echo translate('customer_input_title'); ?>">'
            +'    </div>'
            +'    <div class="col-sm-5">'
            +'        <select class="demo-chosen-select op_type required" name="op_type[]" >'
            +'            <option value="">(none)</option>'
            +'            <option value="text">Text Input</option>'
            +'            <option value="single_select">Dropdown Single Select</option>'
            +'            <option value="multi_select">Dropdown Multi Select</option>'
            +'            <option value="radio">Radio</option>'
            +'        </select>'
            +'        <div class="col-sm-12 options">'
            +'          <input type="hidden" name="op_set'+co+'[]" value="none" >'
            +'        </div>'
            +'    </div>'
            +'    <input type="hidden" name="op_no[]" value="'+co+'" >'
            +'    <div class="col-sm-2">'
            +'        <span class="remove_it_o rmo btn btn-danger btn-icon btn-circle icon-lg fa fa-times" onclick="delete_row(this)"></span>'
            +'    </div>'
            +'</div>'
        );
        set_select();
    });
    
    $("#more_additional_options").on('change','.op_type',function(){
        var co = $(this).closest('.form-group').data('no');
        if($(this).val() !== 'text' && $(this).val() !== ''){
            $(this).closest('div').find(".options").html(''
                +'    <div class="col-sm-12">'
                +'        <div class="col-sm-12 options margin-bottom-10"></div><br>'
                +'        <div class="btn btn-mint btn-labeled fa fa-plus pull-right add_op">'
                +'        <?php echo translate('add_options_for_choice');?></div>'
                +'    </div>'
            );
        } else if ($(this).val() == 'text' || $(this).val() == ''){
            $(this).closest('div').find(".options").html(''
                +'    <input type="hidden" name="op_set'+co+'[]" value="none" >'
            );
        }
    });
    
    $("#more_additional_options").on('click','.add_op',function(){
        var co = $(this).closest('.form-group').data('no');
        $(this).closest('.col-sm-12').find(".options").append(''
            +'    <div>'
            +'        <div class="col-sm-10">'
            +'          <input type="text" name="op_set'+co+'[]" class="form-control required"  placeholder="<?php echo translate('option_name'); ?>">'
            +'        </div>'
            +'        <div class="col-sm-2">'
            +'          <span class="remove_it_n rmon btn btn-danger btn-icon btn-circle icon-sm fa fa-times" onclick="delete_row(this)"></span>'
            +'        </div>'
            +'    </div>'
        );
    });
    
    $('body').on('click', '.rmo', function(){
        $(this).parent().parent().remove();
    });

    $('body').on('click', '.rmon', function(){
        var co = $(this).closest('.form-group').data('no');
        $(this).parent().parent().remove();
        if($(this).parent().parent().parent().html() == ''){
            $(this).parent().parent().parent().html(''
                +'   <input type="hidden" name="op_set'+co+'[]" value="none" >'
            );
        }
    });

    $('body').on('click', '.rms', function(){
        $(this).parent().parent().remove();
    });

    $("#more_color_btn").click(function(){
        $("#more_colors").append(''
            +'      <div class="col-md-12" style="margin-bottom:8px;">'
            +'          <div class="col-md-10">'
            +'              <div class="input-group demo2">'
			+'		     	   <input type="text" value="#ccc" name="color[]" class="form-control" />'
			+'		     	   <span class="input-group-addon"><i></i></span>'
			+'		        </div>'
            +'          </div>'
            +'          <span class="col-md-2">'
            +'              <span class="remove_it_v rmc btn btn-danger btn-icon icon-lg fa fa-trash" ></span>'
            +'          </span>'
            +'      </div>'
  		);
		createColorpickers();
    });		           

    $('body').on('click', '.rmc', function(){
        $(this).parent().parent().remove();
    });


	$(document).ready(function() {
		$("form").submit(function(e){
			event.preventDefault();
		});
	});
</script>

<style>
	.btm_border{
		border-bottom: 1px solid #ebebeb;
		padding-bottom: 15px;	
	}
    .exam_text_area{
        width: 100%;
    }
</style>


<!--Bootstrap Tags Input [ OPTIONAL ]-->

