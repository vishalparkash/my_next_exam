<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" >
        <thead>
            <tr>
                <th><?php echo translate('book_name');?></th>
                <th><?php echo translate('Author');?></th>
                <th><?php echo translate('Added by');?></th>
                <th><?php echo translate('User email');?></th>
                <th><?php echo translate('status');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>				
        <tbody >
        <?php
            $i = 0;
            foreach($all_books as $row){
                $i++;
                
                  $l = $this->db->get_where('users', array(
               'user_id' => $row['user_id']
            ));
            $n = $l->num_rows();
           
            if ($n > 0) {
               // print_r(ucfirst($l->row()->first_name." ".$l->row()->last_name));die;
                $user_name =  ucfirst($l->row()->first_name." ".$l->row()->last_name);
                $user_email =  $l->row()->user_email;
            }else{
                return "None";
            }
                
        ?>                
        <tr>
           
            <td><?php echo ucfirst($row['book_name']); ?></td>
            <td><?php echo $row['author']; ?></td>
            
            <?php /*<td><?php echo get_user_name_by_id('user', $row['user_id'], 'users')?></td> */?>
            <td><?php echo $user_name?></td>
            <td><a href="#"><?php echo $user_email?></a></td>
            <td>
            	<div class="label label-<?php if($row['status'] == 'Active'){ ?>purple<?php } else { ?>danger<?php } ?>">
                	<?php if($row['status'] == 'Active') {echo 'Approved';}else{ echo "Rejected";} ?>
                </div>
            </td>
            <td class="text-right">
                <a class="btn btn-dark btn-xs btn-labeled fa fa-user" data-toggle="tooltip" 
                    onclick="ajax_modal('view','<?php echo translate('Book Details'); ?>','<?php echo translate('successfully_viewed!'); ?>','vendor_view','<?php echo $row['book_id']; ?>')" data-original-title="View" data-container="body">
                        <?php echo translate('Book Details');?>
                </a>
                <a class="btn btn-success btn-xs btn-labeled fa fa-check" data-toggle="tooltip" 
                    onclick="ajax_modal('approval','<?php echo translate('Book Approval'); ?>','<?php echo translate('successfully_viewed!'); ?>','vendor_approval','<?php echo $row['book_id']; ?>')" data-original-title="View" data-container="body">
                        <?php echo translate('approval');?>
                </a>
                
                <a onclick="delete_confirm('<?php echo $row['book_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-xs btn-danger btn-labeled fa fa-trash" data-toggle="tooltip" 
                    data-original-title="Delete" data-container="body">
                        <?php echo translate('delete');?>
                </a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
    <div id="vendr"></div>
    <div id='export-div' style="padding:40px;">
		<h1 id ='export-title' style="display:none;"><?php echo translate('Books'); ?></h1>
		
	</div>
           