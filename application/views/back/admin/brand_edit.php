<?php
//error_reporting(0);
	foreach($brand_data as $row){
	//echo "<pre>";
		//print_r($row);die;
?>
    <div>
        <?php
			echo form_open(base_url() . 'index.php/admin/brand/update/' . $row['sub_course_id'], array(
				'class' => 'form-horizontal',
				'method' => 'post',
				'id' => 'brand_edit',
				'enctype' => 'multipart/form-data'
			));
		?>
            <div class="panel-body">

                <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('Course');?></label>
                <div class="col-sm-6">
                    <input type="text" name="course_name" id="demo-hor-1" 
                    	placeholder="<?php echo translate('Course name'); ?>" class="form-control required" value="<?php echo $row['course_name'];?>">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('Full Course Name');?></label>
                <div class="col-sm-6">
                    <input type="text" name="full_name" id="demo-hor-1" 
                    	placeholder="<?php echo translate('Full Course Name'); ?>" class="form-control required" value="<?php echo $row['introduction'];?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo translate('Sub Courses');?></label>
                <div class="col-sm-6">
					<select name="courses[]" multiple class="course_select">
						
						
						<?php $all = $this->crud_model->get_sub_courses_all();
							foreach($all as $rows){ ?>
								<option value='<?php echo $rows['course_id'];?>'><?php echo $rows['course_name'];?></option>
							<?php }
						?>
						
					</select>
                    <?php //echo $this->crud_model->select_html('courses','course','course_name','add','demo-chosen-select required','','','multi'); ?>
                </div>
            </div>
		    <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo translate('Category');?></label>
                <div class="col-sm-6">
                    <?php echo $this->crud_model->select_html('course_category','course_category','category_name','edit','demo-chosen-select required',$row['category_id'],'',NULL); ?>
                </div>
            </div>
			<div class="form-group">
                                <label class="col-sm-4 control-label"><?php echo translate('Duration');?></label>
                                <div class="col-sm-6">
                                <select name="duration" id="duration" class="demo-chosen-select required" tabindex="2">
                                    <option value="2 Years" <?php if($row['duration']=='2 Years') echo "selected";?>>2 Years</option>
                                    <option value="3 Years" <?php if($row['duration']=='3 Years') echo "selected";?>>3 Years</option>
                                    <option value="4 Years" <?php if($row['duration']=='4 Years') echo "selected";?>>4 Years</option>
                                </select>
                                </div>
                            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('Eligibility');?></label>
                <div class="col-sm-6">
                    <input type="text" name="eligibility" id="demo-hor-1" 
                    	placeholder="<?php echo translate('Eligibility'); ?>" class="form-control required"  value='<?php echo $row['eligibility'];?>'>
                </div>
            </div>
			<div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('description'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9"  class="summernotes" name="description" data-height="200" data-name="description"><?php echo $row['description'];?></textarea>
                                </div>
                            </div>
             
            </div>
        </form>
    </div>

<?php
	}
?>

<script src="<?php echo base_url(); ?>template/back/js/custom/brand_form.js"></script>

<style>
.course_select{width: 100%;}
.summernotes{width: 100%;}
</style>
