<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow" ><?php echo translate('Send Global Notification')?></h1>
	</div>
	<div class="tab-base">
		<!--Tabs Content-->
		<div class="panel">
		<!--Panel heading-->
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane fade active in" id="lista">
						<div class="panel-body" id="demo_s">
    <?php
		echo form_open(base_url() . 'index.php/admin/newsletter/send/', array(
			'class' => 'form-horizontal',
			'method' => 'post',
			'id' => 'stock_add',
			'enctype' => 'multipart/form-data'
		));
	?>
        <div class="panel-body">
			
			  <div class="form-group">
                <label class="col-sm-4 control-label" for="title"><?php echo translate('title');?></label>
                <div class="col-sm-6">
                    <input type="text" name="title" id="title" class="form-control totals required">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="notification_type"><?php echo translate('notification_type');?></label>
                <div class="col-sm-6">
                   <select name="notification_type" class="demo-chosen-select required" id="notification_type" onchange="set_notification_type();">
					  
					   <option value="1">Global Notification</option>
					   <option value="2">Examwise Notification</option>
				   	
					</select>
                </div>
            </div>
			
			
            <div class="form-group" id="sub" style="display:none;">
                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('category');?></label>
                <div class="col-sm-6" id="sub_cat">
                    <?php //echo $this->crud_model->select_html('entrance_exams','exam','exam_name','add','demo-chosen-select required','','',NULL,''); ?>
                </div>
            </div>

            <div class="form-group" id="sub2" style="display:none;">
                <label class="col-sm-4 control-label" for="demo-hor-2"><?php echo translate('sub_category');?></label>
                <div class="col-sm-6" id="sub_cat2">
                </div>
            </div>

          

            

           

            

            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-7"><?php echo translate('reason_note');?></label>
                <div class="col-sm-6">
                    <textarea name="text" class="form-control" rows="3"></textarea>
                </div>
            </div>
			<div class="panel-footer text-right">
	                                <span class="btn btn-info submitter enterer"  data-ing='<?php echo translate('sending'); ?>' data-msg='<?php echo translate('sent!'); ?>'>
										<?php echo translate('send')?>
                                        	</span>
	                            </div>
        </div>
	</div>
				</div>
			</div>
		</div>
	</div>

</div>
<script type="text/javascript">

    $(document).ready(function() {
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
    });

    function other(){
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
        $('#reserve').hide();
        $('#rate').val($('#reserve').html());
        total();
    }
    function get_cat(id){
        $('#sub').hide('slow');
		$('#pro').hide('slow');
        ajax_load(base_url+'index.php/admin/stock/sub_by_cat/'+id,'sub_cat','other');
        $('#sub').show('slow');
        total();
    }
	
	function set_notification_type(){
		if($('#notification_type').val()=='2'){
			 $('#sub').hide('slow');
			//$('#pro').hide('slow');
			ajax_load(base_url+'index.php/admin/newsletter/sub_by_cat/','sub_cat', 'other');
			$('#sub').show('slow');
       
		}
       
    }
	
	//set_notification_type
	function get_product(id){
        $('#pro').hide('slow');
        ajax_load(base_url+'index.php/admin/stock/pro_by_sub/'+id,'product','other');
        $('#pro').show('slow');
        total();
    }
    
    function get_pro_res(id){
        ajax_load(base_url+'index.php/admin/product/pur_by_pro/'+id,'reserve','other');
    }
    function total(){
        var total = Number($('#quantity').val())*Number($('#rate').val());
        $('#total').val(total);
    }
    $(".totals").change(function(){
        total();
    });


	$(document).ready(function() {
		$("form").submit(function(e){
			event.preventDefault();
		});
	});
</script>
<div id="reserve"></div>

