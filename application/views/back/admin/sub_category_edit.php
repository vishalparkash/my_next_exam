<?php
	foreach($sub_category_data as $row){
?>
 
<div>
	<?php
        echo form_open(base_url() . 'index.php/admin/sub_category/update/' . $row['course_id'], array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'sub_category_edit',
            'enctype' => 'multipart/form-data'
        ));
    ?>
        <div class="panel-body">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">
                	<?php echo translate('Sub Course Name');?>
                    	</label>
                <div class="col-sm-6">
                    <input type="text" value="<?php echo $row['course_name'];?>" name="sub_category_name" placeholder="<?php echo translate('Sub Course Name'); ?>" class="form-control required">
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-1">
                	<?php echo translate('Number of Sessions');?>
                    	</label>
                <div class="col-sm-6">
                    <input type="text" value="<?php echo $row['total_sessions'];?>" name="total_sessions" placeholder="<?php echo translate('Number of Sessions'); ?>" class="form-control required">
                </div>
            </div>
          
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo translate('Stream');?></label>
                <div class="col-sm-6">
                    <?php echo $this->crud_model->select_html('streams','stream','stream_name','edit','demo-chosen-select required','','',NULL); ?>
                </div>
            </div>
              <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-2">
                    <?php echo translate('Sub Course Icon');?>
                </label>
                <div class="col-sm-6">
                    <span class="pull-left btn btn-default btn-file">
                        <?php echo translate('Select Sub Course Icon');?>
                        <input type="file" name="icon" id='imgInp' accept="image">
                    </span>
                    <br><br>
                    <span id='wrap' class="pull-left" >
                         <?php
							if(file_exists('uploads/sub_course_image/'.$row['icon'])){
						?>
						<img src="<?php echo base_url(); ?>uploads/sub_course_image/<?php echo $row['icon']; ?>" width="100%" id='blah' />  
						<?php
							} else {
						?>
						<img src="<?php echo base_url(); ?>uploads/sub_course_image/default.jpg" width="100%" id='blah' />
						<?php
							}
						?>
                    </span>
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-4 control-label" for="demo-hor-2">
                    <?php echo translate('Image for Book Store');?>
                </label>
                <div class="col-sm-6">
                    <span class="pull-left btn btn-default btn-file">
                        <?php echo translate('Select Image for Book Store');?>
                        <input type="file" name="img" id='imgInp' accept="image">
						
                    </span>
                    <br><br>
                    <span id='wrap' class="pull-left" >
                         <?php
							if(file_exists('uploads/sub_course_image/'.$row['image_for_book_store'])){
						?>
						<img src="<?php echo base_url(); ?>uploads/sub_course_image/<?php echo $row['image_for_book_store']; ?>" width="100%" id='blah' />  
						<?php
							} else {
						?>
						<img src="<?php echo base_url(); ?>uploads/sub_course_image/default.jpg" width="100%" id='blah' />
						<?php
							}
						?>
                    </span>
                </div>
            </div>
			
        </div>
    </form>
</div>
<?php
	}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
    });


	$(document).ready(function() {
		$("form").submit(function(e){
			event.preventDefault();
		});
	});
	
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
	
			reader.onload = function(e) {
				$('#wrap').hide('fast');
				$('#blah').attr('src', e.target.result);
				$('#wrap').show('fast');
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	$("#imgInp").change(function() {
		readURL(this);
	});
</script>	
