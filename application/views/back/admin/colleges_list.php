<?php error_reporting(0);?>	
<div class="panel-body" id="demo_s">
		<table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" >

			<thead>
				<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('college_name');?></th>
						<th><?php echo translate('Establishment Year');?></th>
						<th><?php echo translate('university');?></th>
						<th><?php echo translate('city');?></th>
						<th><?php echo translate('state');?></th>
						<th><?php //echo translate('logo');?></th>
						<th><?php echo translate('options');?></th>
					</tr>
				</thead>
				
			<tbody >
			<?php
				$i=0;
            	foreach($all_colleges as $row){
					/*echo "<pre>";
					print_r(count($row));die;*/
            		$i++;
			?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <!--td >
                        <?php
							//if(file_exists('uploads/brand_image/'.$row['logo'])){
						?>
						<img class="img-md" src="<?php// echo base_url(); ?>uploads/brand_image/<?php// echo $row['logo']; ?>" />  
						<?php
							//} else {
						?>
						<img class="img-md" src="<?php //echo base_url(); ?>uploads/brand_image/default.jpg" />
						<?php
							///}
						?> 
                    </td-->
                    <td><?php echo $row['college_name']; ?></td>
                    <td><?php echo $row['estd_year']; ?></td>
                    <td><?php echo $row['university']; ?></td>
                    <td><?php echo $row['city']; ?></td>
                    <td><?php echo $row['state']; ?></td>
                    <td><?php //echo $row['logo_url']; ?></td>
                    <td class="text-right">
                        <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
                            onclick="ajax_modal('edit','<?php echo translate('Edit Colleges'); ?>','<?php echo translate('successfully_edited!'); ?>','colleges_edit','<?php echo $row['college_id']; ?>')" 
                                data-original-title="Edit" 
                                    data-container="body"><?php echo translate('edit');?>
                        </a>
                        
                        <a onclick="delete_confirm('<?php echo $row['college_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" 
                            class="btn btn-danger btn-xs btn-labeled fa fa-trash" 
                                data-toggle="tooltip" data-original-title="Delete" 
                                    data-container="body"><?php echo translate('delete');?>
                        </a>
                        
                    </td>
                </tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>
           
	<!--div id='export-div'>
		<h1 style="display:none;"><?php //echo translate('brand'); ?></h1>
		<table id="export-table" data-name='brand' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th><?php //echo translate('no');?></th>
						<th><?php //echo translate('name');?></th>
						<th><?php //echo translate('category');?></th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					/*$i = 0;
	            	foreach($all_brands as $row){
	            		$i++;*/
				?>
				<tr>
					<td><?php //echo $i; ?></td>
					<td><?php //echo $row['name']; ?></td>
					<td><?php //echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></td>
				</tr>
	            <?php
	            	//}
				?>
				</tbody>
		</table>
	</div-->

<style>
	.highlight{
		background-color: #E7F4FA;
	}
</style>







           